function points = depth(path, count)
    image = imread(path);
    h_f = figure;
    set(h_f, 'KeyPressFcn', @on_key_press);
    h_i = imshow(image);
    set(h_i, 'ButtonDownFcn', @on_mouse_click);
    next = false;
    point = [-1, -1];
    hold on;
    h_p = plot(0, 0, '+m');
    delete(h_p);
    points = zeros(count, 2);
    for i = 1:count
        next = false;
        while next == false
            pause(0.016);
        end
        points(i, :) = point;
        h_p = plot(0, 0, '+m');
        delete(h_p);
        fprintf('%d/%d\n', i, count);
    end
    hold off;
    
    function on_key_press(handle, event)
        if strcmp(event.Key, 'return')
            next = true;
        else
            next = false;
        end
    end

    function on_mouse_click(handle, event)
        if next == true
            return;
        end
        delete(h_p);
        point = get(handle.Parent, 'CurrentPoint');
        point = [point(1), point(3)];
        h_p = plot(point(1), point(2), '+m');
    end
end

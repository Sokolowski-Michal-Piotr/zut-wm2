function f = debug_draw(image, landmarks, triangles, pts_color, line_color)
    f = figure;
    imshow(image);
    hold on;
    
    if nargin < 4
        pts_color = 'm';
        line_color = 'w';
    end
    
    for j = 1:length(triangles)
        abc = landmarks(triangles(j, :), :);
        a = abc(1, :);
        b = abc(2, :);
        c = abc(3, :);
        line([a(1), b(1)], [a(2), b(2)], 'Color', line_color);
        line([b(1), c(1)], [b(2), c(2)], 'Color', line_color);
        line([c(1), a(1)], [c(2), a(2)], 'Color', line_color);
    end
    
    plot(landmarks(:, 1), landmarks(:, 2), ['+', pts_color]);
    
    hold off;
end


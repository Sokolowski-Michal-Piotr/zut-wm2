clc
close all
clear all
 
N = 256;
depth_path = '.\resources\depth.mat';
images_query = '.\resources\test*.jpg';

interface_path = '.\3rdparty';
predictor_path = '.\3rdparty\shape_predictor_68_face_landmarks.dat';
addpath(interface_path, '-end');
 
temp = rdir(images_query);
image_paths = { temp(:).name };
 
if ~isempty(strfind(depth_path, '.mat'))
    load(depth_path);
else
    [depths, depthmarks, depth_width] = depth2(predictor_path, image_paths{1}, depth_path);
end
 
for i = 1:length(image_paths)
    image = imread(image_paths{i});
    result = find_face_landmarks(predictor_path, image_paths{i});
    try
        landmarks = double(result.faces.landmarks);
    catch e
        fprintf('Could not detect landmarks for image: %s\n', image_paths{i});
        continue;
    end
    xs = landmarks(:, 1);
    ys = landmarks(:, 2);
    zs = depths;
    triangles = delaunay(xs, ys);
    
    %debug_draw(image, landmarks, triangles);
        
    h = figure;
    
    xmin = min(xs);
    xmax = max(xs);
    ymin = min(ys);
    ymax = max(ys);
    xu = linspace(xmin, xmax, N);
    yu = linspace(ymin, ymax, N);
    [X, Y] = meshgrid(xu, yu);
    zs = (xmax - xmin) / depth_width * zs;
    F = TriScatteredInterp(xs, ys, zs);
    Z = F(X,Y);
    texture = image(ymin:ymax, xmin:xmax, :);
    surf(X,Y,Z, texture,'FaceColor','texturemap','EdgeColor','none');
    colormap(gray);
    
    axis equal;
    waitfor(h);
end

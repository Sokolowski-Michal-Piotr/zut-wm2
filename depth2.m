function [depths, points_out, width] = depth2(predictor_path, image_path, depth_path)
    image = imread(image_path);
    depth = imread(depth_path);

    h_f = figure;
    set(h_f, 'KeyPressFcn', @on_key_press);
    
    subplot(1, 2, 1);
    imshow(image);
    hold on;
    
    temp = find_face_landmarks(predictor_path, image_path);
    points_src = double(temp.faces.landmarks);
    offset = repmat(peak2peak(points_src) * 0.2, [1, 2]) .* [-1, 1, -1, 1];
    xlim(offset(1:2) + [min(points_src(:,1)), max(points_src(:,1))]);
    ylim(offset(3:4) + [min(points_src(:,2)), max(points_src(:,2))]);
    
    subplot(1, 2, 2);
    h_i = imshow(depth);
    hold on;
    set(h_i, 'ButtonDownFcn', @on_mouse_click);
    
    next = false;
    point = [-1, -1];
    h_p = plot(0, 0, '+m');
    delete(h_p);    

    depths = zeros(length(points_src), 1);
    points_out = zeros(size(points_src));
    for i = 1:length(points_out)
        subplot(1, 2, 1);
        plot(points_src(i, 1), points_src(i, 2), '+m');
        subplot(1, 2, 2);
        next = false;
        while next == false
            pause(0.016);
        end
        points_out(i, :) = point;
        depths(i) = depth(points_out(i, 2), points_out(i, 1));
        h_p = plot(0, 0, '+m');
        delete(h_p);
        fprintf('%d/%d\n', i, length(points_src));
    end
    width = max(points_out(:, 1)) - min(points_out(:, 1));
    hold off;
    
    function on_key_press(handle, event)
        if strcmp(event.Key, 'return')
            next = true;
        else
            next = false;
        end
    end

    function on_mouse_click(handle, event)
        if next == true
            return;
        end
        delete(h_p);
        point = get(handle.Parent, 'CurrentPoint');
        point = [point(1), point(3)];
        point = round(point);
        h_p = plot(point(1), point(2), '+m');
    end
end
